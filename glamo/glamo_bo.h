/*
 * Copyright (c) 2009 Thomas White
 *
 * Heavily based on radeon_bo.h
 * Copyright © 2008 Jérôme Glisse
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sub license, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE COPYRIGHT HOLDERS, AUTHORS
 * AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 */
/*
 * Authors:
 *      Jérôme Glisse <glisse@freedesktop.org>
 *	Thomas White <taw@bitwiz.org.uk>
 */
#ifndef GLAMO_BO_H
#define GLAMO_BO_H

#include <stdio.h>
#include <stdint.h>
#include "glamo_track.h"

/* bo object */
#define GLAMO_BO_FLAGS_MACRO_TILE  1
#define GLAMO_BO_FLAGS_MICRO_TILE  2

struct glamo_bo_manager;

struct glamo_bo {
    uint32_t                    alignment;
    uint32_t                    handle;
    uint32_t                    size;
    uint32_t                    domains;
    uint32_t                    flags;
    unsigned                    cref;
#ifdef GLAMO_BO_TRACK
    struct glamo_track		*track;
#endif
    struct glamo_bo_manager	*bom;
    void			*virtual;
    uint32_t			space_accounted;
};

/* bo functions */
struct glamo_bo_funcs {
    struct glamo_bo *(*bo_open)(struct glamo_bo_manager *bom,
                                 uint32_t handle,
                                 uint32_t size,
                                 uint32_t alignment,
                                 uint32_t domains,
                                 uint32_t flags);
    void (*bo_ref)(struct glamo_bo *bo);
    struct glamo_bo *(*bo_unref)(struct glamo_bo *bo);
    int (*bo_map)(struct glamo_bo *bo, int write);
    int (*bo_unmap)(struct glamo_bo *bo);
    int (*bo_wait)(struct glamo_bo *bo);
};

struct glamo_bo_manager {
    struct glamo_bo_funcs  *funcs;
    int                     fd;
    struct glamo_tracker   tracker;
};

static inline void _glamo_bo_debug(struct glamo_bo *bo,
                                    const char *op,
                                    const char *file,
                                    const char *func,
                                    int line)
{
    fprintf(stderr, "%s %p 0x%08X 0x%08X 0x%08X [%s %s %d]\n",
            op, (void *)bo, bo->handle, bo->size, bo->cref, file, func, line);
}

static inline struct glamo_bo *_glamo_bo_open(struct glamo_bo_manager *bom,
                                                uint32_t handle,
                                                uint32_t size,
                                                uint32_t alignment,
                                                uint32_t domains,
                                                uint32_t flags,
                                                const char *file,
                                                const char *func,
                                                int line)
{
    struct glamo_bo *bo;

    bo = bom->funcs->bo_open(bom, handle, size, alignment, domains, flags);
#ifdef GLAMO_BO_TRACK
    if (bo) {
        bo->track = glamo_tracker_add_track(&bom->tracker, bo->handle);
        glamo_track_add_event(bo->track, file, func, "open", line);
    }
#endif
    return bo;
}

static inline void _glamo_bo_ref(struct glamo_bo *bo,
                                  const char *file,
                                  const char *func,
                                  int line)
{
    bo->cref++;
#ifdef GLAMO_BO_TRACK
    glamo_track_add_event(bo->track, file, func, "ref", line);
#endif
    bo->bom->funcs->bo_ref(bo);
}

static inline struct glamo_bo *_glamo_bo_unref(struct glamo_bo *bo,
                                                 const char *file,
                                                 const char *func,
                                                 int line)
{
    bo->cref--;
#ifdef GLAMO_BO_TRACK
    glamo_track_add_event(bo->track, file, func, "unref", line);
    if (bo->cref <= 0) {
        glamo_tracker_remove_track(&bo->bom->tracker, bo->track);
        bo->track = NULL;
    }
#endif
    return bo->bom->funcs->bo_unref(bo);
}

static inline int _glamo_bo_map(struct glamo_bo *bo,
                                 int write,
                                 const char *file,
                                 const char *func,
                                 int line)
{
    return bo->bom->funcs->bo_map(bo, write);
}

static inline int _glamo_bo_unmap(struct glamo_bo *bo,
                                   const char *file,
                                   const char *func,
                                   int line)
{
    return bo->bom->funcs->bo_unmap(bo);
}

static inline int _glamo_bo_wait(struct glamo_bo *bo,
                                  const char *file,
                                  const char *func,
                                  int line)
{
    return bo->bom->funcs->bo_wait(bo);
}

#define glamo_bo_open(bom, h, s, a, d, f)\
    _glamo_bo_open(bom, h, s, a, d, f, __FILE__, __FUNCTION__, __LINE__)
#define glamo_bo_ref(bo)\
    _glamo_bo_ref(bo, __FILE__, __FUNCTION__, __LINE__)
#define glamo_bo_unref(bo)\
    _glamo_bo_unref(bo, __FILE__, __FUNCTION__, __LINE__)
#define glamo_bo_map(bo, w)\
    _glamo_bo_map(bo, w, __FILE__, __FUNCTION__, __LINE__)
#define glamo_bo_unmap(bo)\
    _glamo_bo_unmap(bo, __FILE__, __FUNCTION__, __LINE__)
#define glamo_bo_debug(bo, opcode)\
    _glamo_bo_debug(bo, opcode, __FILE__, __FUNCTION__, __LINE__)
#define glamo_bo_wait(bo) \
    _glamo_bo_wait(bo, __FILE__, __func__, __LINE__)

#endif
